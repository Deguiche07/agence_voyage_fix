<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use App\Entity\User;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;


use Doctrine\ORM\EntityManagerInterface;

class SetRole extends Command
{
	private $em;

	private static function int_to_role($role_int)
	{
		/* Fonction qui convertit l'entier vers le role équivalent
		 * Existe pour des raison de praticité 
		 * Si Erreur dans saisi de l'entier, retourne "ROLE_USER" par défaut
		 */
		if ($role_int == 1) return "ROLE_USER";
		elseif ($role_int == 2) return "ROLE_ADMIN";
		else return "ROLE_USER";
	}

	private static function str_to_role($role)
	{
		/* Fonction qui check si le role en entrée existe
		 * Retour ROLE_USER en cas d'erreur
		 */
		if ($role != "ROLE_USER" && $role != "ROLE_MODO" && $role != "ROLE_ADMIN")
			return "ROLE_USER";
		else return $role;
	}

	public function __construct(EntityManagerInterface $em){
		parent::__construct();
		$this->em = $em;
	}
	protected function configure()
	{
		$this->setName('app:setRole');
		$this->setDescription("Permet de changer le rôle d'un utilisateur dans la BDD")
			->setHelp("Prend deux arguments, le nom (username) de l'utilisateur, le role voulu ou une équivalence en entier\n
				ROLE_USER : 1\n
				ROLE_ADMIN : 2\n")

			->addArgument('user', InputArgument::REQUIRED, "Quel utilisateur ?")
			->addArgument('role', InputArgument::REQUIRED, "Quel roles ? ['ROLE_USER':1,'ROLE_ADMIN':2]");
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$user = $input->getArgument('user');
		$role = $input->getArgument('role');

		//check si User existe
		$userRepo = $this->em->getRepository("App:User")->findBy(['login'=>$user]);
		
		if (count($userRepo)== 0){
			print_r("Pas d'utilisateur trouvé pour ".$user."\n");
			return Command::FAILURE;
		} elseif (count($userRepo) > 1){
			print_r("Plusieur utilisateur trouvé pour ".$user."\n");
			return Command::FAILURE;
		} else {
			$userObjet = $userRepo[0];
		}

		if (is_numeric($role)) {$role = $this::int_to_role($role);}
		else {$role = $this::str_to_role($role);}

		// Ok pour changement de role
		print_r("Ok pour changement de role de : ".$user." en ".$role."\n");
		$userObjet->setRoles([$role]);
		$this->em->persist($userObjet);
		$this->em->flush();

		print_r("Changement effectué en BDD\n");

		return Command::SUCCESS;

	}
}