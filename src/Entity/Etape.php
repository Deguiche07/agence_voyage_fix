<?php

namespace App\Entity;

use App\Repository\EtapeRepository;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource(
 *     collectionOperations={
 *      "get"={"security"="is_granted('ROLE_ADMIN')"},
 *      "post"={"security"="is_granted('ROLE_ADMIN')"}
 *      },
 *     itemOperations={
 *      "get",
 *      "put"={"security"="is_granted('ROLE_ADMIN')"},
 *      "delete"={"security"="is_granted('ROLE_ADMIN')"}
 *     }
 * )
 * @ORM\Entity(repositoryClass=EtapeRepository::class)
 */
class Etape
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"voyage:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"voyage:read"})
     */
    private $titre;

    /**
     * @ORM\Column(type="text")
     * @Groups({"voyage:read"})
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"voyage:read"})
     */
    private $photoUn;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"voyage:read"})
     */
    private $photoDeux;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"voyage:read"})
     */
    private $photoTrois;

    /**
     * @ORM\Column(type="date")
     * @Groups({"voyage:read"})
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity=Voyage::class, inversedBy="etapes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $voyage;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"voyage:read"})
     */
    private $numEtape;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPhotoUn(): ?string
    {
        return $this->photoUn;
    }

    public function setPhotoUn(?string $photoUn): self
    {
        $this->photoUn = $photoUn;

        return $this;
    }

    public function getPhotoDeux(): ?string
    {
        return $this->photoDeux;
    }

    public function setPhotoDeux(?string $photoDeux): self
    {
        $this->photoDeux = $photoDeux;

        return $this;
    }

    public function getPhotoTrois(): ?string
    {
        return $this->photoTrois;
    }

    public function setPhotoTrois(?string $photoTrois): self
    {
        $this->photoTrois = $photoTrois;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getVoyage(): ?Voyage
    {
        return $this->voyage;
    }

    public function setVoyage(?Voyage $voyage): self
    {
        $this->voyage = $voyage;

        return $this;
    }

    public function getNumEtape(): ?int
    {
        return $this->numEtape;
    }

    public function setNumEtape(int $numEtape): self
    {
        $this->numEtape = $numEtape;

        return $this;
    }
}
