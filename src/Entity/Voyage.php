<?php

namespace App\Entity;

use App\Repository\VoyageRepository;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource(
 *     collectionOperations={
 *      "get",
 *      "post"={"security"= "is_granted('ROLE_ADMIN')"}
 *     },
 *     itemOperations={
 *      "get",
 *      "put"={"security"= "is_granted('ROLE_ADMIN')"},
 *      "delete"={"security"= "is_granted('ROLE_ADMIN')"}
 *      },
 *     normalizationContext={"groups"={"voyage:read"}}
 * )
 * @ORM\Entity(repositoryClass=VoyageRepository::class)
 */
class Voyage
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"voyage:read", "reserver:read", "user:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"voyage:read", "reserver:read", "user:read"})
     */
    private $titre;

    /**
     * @ORM\Column(type="text")
     * @Groups({"voyage:read", "reserver:read", "user:read"})
     */
    private $description;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"voyage:read", "reserver:read", "user:read"})
     */
    private $dateDebut;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"voyage:read", "reserver:read", "user:read"})
     */
    private $dateFin;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"voyage:read", "reserver:read", "user:read"})
     */
    private $prixInitial;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"voyage:read", "reserver:read", "user:read"})
     */
    private $nbPlacesDisponibles;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"voyage:read", "reserver:read", "user:read"})
     */
    private $photoUn;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"voyage:read", "reserver:read", "user:read"})
     */
    private $photoDeux;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"voyage:read", "reserver:read", "user:read"})
     */
    private $photoTrois;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"voyage:read", "reserver:read", "user:read"})
     */
    private $photoQuatre;

    // /**
    //  * @ORM\ManyToMany(targetEntity=Option::class)
    //  * @Groups({"voyage:read"})
    //  */
    // private $options;

    /**
     * @ORM\OneToMany(targetEntity=Reserver::class, mappedBy="voyage", orphanRemoval=true)
     * @Groups({"voyage:read"})
     */
    private $reservations;

    /**
     * @ORM\OneToMany(targetEntity=Etape::class, mappedBy="voyage", orphanRemoval=true)
     * @Groups({"voyage:read"})
     */
    private $etapes;

    /**
     * @ORM\Column(type="boolean")
     */
    private $lead;

    /**
     * @ORM\ManyToMany(targetEntity=Option::class, inversedBy="voyages")
     * @Groups({"voyage:read"})
     */
    private $options;

    public function __construct()
    {
        $this->reservations = new ArrayCollection();
        $this->etapes = new ArrayCollection();
        $this->options = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDateDebut(): ?\DateTimeInterface
    {
        return $this->dateDebut;
    }

    public function setDateDebut(\DateTimeInterface $dateDebut): self
    {
        $this->dateDebut = $dateDebut;

        return $this;
    }

    public function getDateFin(): ?\DateTimeInterface
    {
        return $this->dateFin;
    }

    public function setDateFin(\DateTimeInterface $dateFin): self
    {
        $this->dateFin = $dateFin;

        return $this;
    }

    public function getPrixInitial(): ?int
    {
        return $this->prixInitial;
    }

    public function setPrixInitial(int $prixInitial): self
    {
        $this->prixInitial = $prixInitial;

        return $this;
    }

    public function getNbPlacesDisponibles(): ?int
    {
        return $this->nbPlacesDisponibles;
    }

    public function setNbPlacesDisponibles(int $nbPlacesDisponibles): self
    {
        $this->nbPlacesDisponibles = $nbPlacesDisponibles;

        return $this;
    }

    public function getPhotoUn(): ?string
    {
        return $this->photoUn;
    }

    public function setPhotoUn(?string $photoUn): self
    {
        $this->photoUn = $photoUn;

        return $this;
    }

    public function getPhotoDeux(): ?string
    {
        return $this->photoDeux;
    }

    public function setPhotoDeux(?string $photoDeux): self
    {
        $this->photoDeux = $photoDeux;

        return $this;
    }

    public function getPhotoTrois(): ?string
    {
        return $this->photoTrois;
    }

    public function setPhotoTrois(?string $photoTrois): self
    {
        $this->photoTrois = $photoTrois;

        return $this;
    }

    public function getPhotoQuatre(): ?string
    {
        return $this->photoQuatre;
    }

    public function setPhotoQuatre(?string $photoQuatre): self
    {
        $this->photoQuatre = $photoQuatre;

        return $this;
    }

    // /**
    //  * @return Collection|Option[]
    //  */
    // public function getOptions(): Collection
    // {
    //     return $this->options;
    // }

    // public function addOption(Option $option): self
    // {
    //     if (!$this->options->contains($option)) {
    //         $this->options[] = $option;
    //     }

    //     return $this;
    // }

    // public function removeOption(Option $option): self
    // {
    //     $this->options->removeElement($option);

    //     return $this;
    // }

    /**
     * @return Collection|Reserver[]
     */
    public function getReservations(): Collection
    {
        return $this->reservations;
    }

    public function addReservation(Reserver $reservation): self
    {
        if (!$this->reservations->contains($reservation)) {
            $this->reservations[] = $reservation;
            $reservation->setVoyage($this);
        }

        return $this;
    }

    public function removeReservation(Reserver $reservation): self
    {
        if ($this->reservations->removeElement($reservation)) {
            // set the owning side to null (unless already changed)
            if ($reservation->getVoyage() === $this) {
                $reservation->setVoyage(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Etape[]
     */
    public function getEtapes(): Collection
    {
        return $this->etapes;
    }

    public function addEtape(Etape $etape): self
    {
        if (!$this->etapes->contains($etape)) {
            $this->etapes[] = $etape;
            $etape->setVoyage($this);
        }

        return $this;
    }

    public function removeEtape(Etape $etape): self
    {
        if ($this->etapes->removeElement($etape)) {
            // set the owning side to null (unless already changed)
            if ($etape->getVoyage() === $this) {
                $etape->setVoyage(null);
            }
        }

        return $this;
    }

    public function getLead(): ?bool
    {
        return $this->lead;
    }

    public function setLead(?bool $lead): self
    {
        $this->lead = $lead;

        return $this;
    }

    /**
     * @return Collection|Option[]
     */
    public function getOptions(): Collection
    {
        return $this->options;
    }

    public function addOption(Option $option): self
    {
        if (!$this->options->contains($option)) {
            $this->options[] = $option;
        }

        return $this;
    }

    public function removeOption(Option $option): self
    {
        $this->options->removeElement($option);

        return $this;
    }
}
