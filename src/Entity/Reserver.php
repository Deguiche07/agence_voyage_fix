<?php

namespace App\Entity;

use App\Repository\ReserverRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource(
 *     collectionOperations={
 *      "get"={"security"="is_granted('ROLE_ADMIN')"}, 
 *      "post"={"security"="is_granted('ROLE_USER')"}
 *      },
 *     itemOperations={
 *      "get"={"security"="is_granted('ROLE_ADMIN') or object.user == user"},
 *      "put"={"security"="is_granted('ROLE_ADMIN') or object.user == user"},
 *      "delete"={"security"="is_granted('ROLE_ADMIN') or object.user == user"}
 *      },
 *     normalizationContext={"groups"={"reserver:read"}}
 * )
 * @ORM\Entity(repositoryClass=ReserverRepository::class)
 */
class Reserver
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"reserver:read", "user:read", "voyage:read"})
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="reservations")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"reserver:read", "voyage:read"})
     */
    public $user;

    /**
     * @ORM\ManyToOne(targetEntity=Voyage::class, inversedBy="reservations")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"reserver:read", "user:read"})
     */
    private $voyage;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"reserver:read", "user:read", "voyage:read"})
     */
    private $paiement_autorise;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"reserver:read", "user:read", "voyage:read"})
     */
    private $paiement_ok;

    /**
     * @ORM\ManyToMany(targetEntity=Option::class, inversedBy="reservers")
     * @Groups({"reserver:read", "user:read"})
     */
    private $options;

    public function __construct()
    {
        $this->options = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getVoyage(): ?Voyage
    {
        return $this->voyage;
    }

    public function setVoyage(?Voyage $voyage): self
    {
        $this->voyage = $voyage;

        return $this;
    }

    public function getPaiementAutorise(): ?bool
    {
        return $this->paiement_autorise;
    }

    public function setPaiementAutorise(?bool $paiement_autorise): self
    {
        $this->paiement_autorise = $paiement_autorise;

        return $this;
    }

    public function getPaiementOk(): ?bool
    {
        return $this->paiement_ok;
    }

    public function setPaiementOk(bool $paiement_ok): self
    {
        $this->paiement_ok = $paiement_ok;

        return $this;
    }

    /**
     * @return Collection|Option[]
     */
    public function getOptions(): Collection
    {
        return $this->options;
    }

    public function addOption(Option $option): self
    {
        if (!$this->options->contains($option)) {
            $this->options[] = $option;
        }

        return $this;
    }

    public function removeOption(Option $option): self
    {
        $this->options->removeElement($option);

        return $this;
    }
}
