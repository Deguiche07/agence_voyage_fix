<?php

namespace App\Entity;

use App\Repository\OptionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource(
 *     collectionOperations={
 *      "get"={"security"="is_granted('ROLE_ADMIN')"},
 *      "post"={"security"="is_granted('ROLE_ADMIN')"}
 *      },
 *     itemOperations={
 *      "get", 
 *      "put"={"security"="is_granted('ROLE_ADMIN')"}, 
 *      "delete"={"security"="is_granted('ROLE_ADMIN')"}
 *      }
 * )
 * @ORM\Entity(repositoryClass=OptionRepository::class)
 * @ORM\Table(name="`option`")
 */
class Option
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"voyage:read", "reserver:read", "user:read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"voyage:read", "reserver:read", "user:read"})
     */
    private $titre;

    /**
     * @ORM\Column(type="text")
     * @Groups({"voyage:read", "reserver:read", "user:read"})
     */
    private $description;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"voyage:read", "reserver:read", "user:read"})
     */
    private $dateDebut;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"voyage:read", "reserver:read", "user:read"})
     */
    private $dateFin;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"voyage:read", "reserver:read", "user:read"})
     */
    private $prix;

    /**
     * @ORM\ManyToMany(targetEntity=Reserver::class, mappedBy="options")
     */
    private $reservers;

    /**
     * @ORM\ManyToMany(targetEntity=Voyage::class, mappedBy="options")
     */
    private $voyages;

    public function __construct()
    {
        $this->reservers = new ArrayCollection();
        $this->voyages = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDateDebut(): ?\DateTimeInterface
    {
        return $this->dateDebut;
    }

    public function setDateDebut(\DateTimeInterface $dateDebut): self
    {
        $this->dateDebut = $dateDebut;

        return $this;
    }

    public function getDateFin(): ?\DateTimeInterface
    {
        return $this->dateFin;
    }

    public function setDateFin(\DateTimeInterface $dateFin): self
    {
        $this->dateFin = $dateFin;

        return $this;
    }

    public function getPrix(): ?int
    {
        return $this->prix;
    }

    public function setPrix(int $prix): self
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * @return Collection|Reserver[]
     */
    public function getReservers(): Collection
    {
        return $this->reservers;
    }

    public function addReserver(Reserver $reserver): self
    {
        if (!$this->reservers->contains($reserver)) {
            $this->reservers[] = $reserver;
            $reserver->addOption($this);
        }

        return $this;
    }

    public function removeReserver(Reserver $reserver): self
    {
        if ($this->reservers->removeElement($reserver)) {
            $reserver->removeOption($this);
        }

        return $this;
    }

    /**
     * @return Collection|Voyage[]
     */
    public function getVoyages(): Collection
    {
        return $this->voyages;
    }

    public function addVoyage(Voyage $voyage): self
    {
        if (!$this->voyages->contains($voyage)) {
            $this->voyages[] = $voyage;
            $voyage->addOption($this);
        }

        return $this;
    }

    public function removeVoyage(Voyage $voyage): self
    {
        if ($this->voyages->removeElement($voyage)) {
            $voyage->removeOption($this);
        }

        return $this;
    }
}
