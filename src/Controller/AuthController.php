<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\User;
use App\Repository\UserRepository;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use \Firebase\JWT\JWT;

class AuthController extends AbstractController
{
    /**
     * @Route("/auth", name="auth")
     */
    public function index(): Response
    {
        return $this->render('auth/index.html.twig', [
            'controller_name' => 'AuthController',
        ]);
    }

    /**
     * @Route("/auth/register", name="register", methods={"POST"})
     */
    public function register(Request $request, UserPasswordEncoderInterface $encoder, UserRepository $userRepo)
    {
        // Comment gérer le cas ou l'utilisateur existe déjà mais essaye de s'enregistrer ?
        // Pour le moment je met une condition mais doit y avoir moyen de faire un try?

        $password = $request->get('password');
        $email = $request->get('email');
        $login = $request->get('login');

        if (!$userRepo->findBy(array('email'=> $email))){
            $user = new User();
            $user->setPassword($encoder->encodePassword($user, $password));
            $user->setEmail($email);
            $user->setLogin($login);
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            return $this->json([
                'user' => $user->getEmail()
            ]);
        } else {
            return $this->json([
                'message' => "L'utilisateur ".$email." existe déjà"
            ]);
        }
    }

    /**
     * @Route("/auth/login", name="login", methods={"POST"})
     */
    public function login(Request $request, UserRepository $userRepository, UserPasswordEncoderInterface $encoder)
    {
            $user = $userRepository->findOneBy([
                'email'=>$request->get('email'),
            ]);
            if (!$user || !$encoder->isPasswordValid($user, $request->get('password'))) {
                    return $this->json([
                        'message' => 'email or password is wrong.',
                        'query'=> $request->query,
                    ]);
            }
        $payload = [
            "id" => $user->getId(),
            "user" => $user->getUsername(),
            "email" => $user->getEmail(),
            "roles" => $user->getRoles(),
            "exp"  => (new \DateTime())->modify("+30 minutes")->getTimestamp(),
        ];


            $jwt = JWT::encode($payload, $this->getParameter('jwt_secret'), 'HS256');
            return $this->json([
                'message' => 'success!',
                'token' => sprintf('Bearer %s', $jwt),
            ]);
    }
}
