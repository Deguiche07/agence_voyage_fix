<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class DefaultController extends AbstractController
{
	/**
	 * @Route("/", name="home", defaults={"reactRouting": null})
	 */
	public function index()
	{
		return $this->render('default/index.html.twig');
	}
	/**
	 * @Route("/home", name="home", defaults={"reactRouting": null})
	 */
	public function home()
	{
		return $this->render('default/index.html.twig');
	}

	
	/**
	 * @Route("/signin" , name="signin" , defaults={"reactRouting": null})
	 */
	public function signin()
	{
		return $this->render('default/index.html.twig');
	}

	/**
	 * @Route("/login" , name="login" , defaults={"reactRouting": null})
	 */
	public function login()
	{
		return $this->render('default/index.html.twig');
	}


	/**
	* @Route("api/tokensetup", name="tokensetup")
	*/
	public function test()
	{
		// $user = $this->getUser();
		return $this->json([
			'message'=>'Connect!'
		]);
	}
}