# Site Organisation de voyages culturels

## Installation

Pour installer et lancer le projet vous devez avoir installé les outils [Composer](https://getcomposer.org), [Symfony CLI](https://symfony.com/download), [Node.js](https://nodejs.org/en/) et [Yarn](https://yarnpkg.com).

1. Exécuter les commandes `composer install` et `yarn install` dans le répertoire contenant le projet.
2. Compiler les ressources avec la commande `yarn dev`.
3. Lancer le site avec `symfony server:start`.

Note : si la commande `yarn` n'est pas disponible vous pouvez utiliser la commande `yarnpkg`.