import React from 'react';
import ReactDOM from 'react-dom';
import Main from './components/Main';
import Signin from './components/Signin';
import Login from './components/Login';
import Paiement from './components/Paiement';
import TravelDetails from './components/TravelDetails';
// import VoyageCreate from './components/admin/VoyageCreate';
import EtapeCreateForm from './components/admin/form/EtapeCreateForm';
import OptionCreateForm from './components/admin/form/OptionCreateForm';
import VoyageCreate from './components/admin/VoyageCreate';

import Profile from './components/user/Profile';
import UserBoard from './components/admin/UserBoard';

import { BrowserRouter as Router} from 'react-router-dom';
import {Route, Switch, Redirect, Link, withRouter} from 'react-router-dom';

import VoyageBoard from './components/admin/VoyageBoard';
import PaiementBoard from './components/admin/PaiementBoard';

ReactDOM.render(
    <Router>
        <Switch>
            <Redirect exact from="/" to="/home"/>
            <Route path="/home" component={Main}/>
            <Route path="/travel-details" component={TravelDetails}/>
            <Route path="/signin" component={Signin}/>
            <Route path="/login" component={Login}/>
            <Route path="/pay" component={Paiement}/>

            <Route path="/navadmin" component={UserBoard}/>

            <Route path="/EtapeCreateForm" component={EtapeCreateForm}/>
            <Route path="/OptionCreateForm" component={OptionCreateForm}/>
            <Route path="/profile" component={Profile}/>

            <Route path="/userboard" component={UserBoard}/>
            <Route path="/voyageboard" component={VoyageBoard}/>
            <Route path="/paiementboard" component={PaiementBoard}/>

            <Route path="/voyagecreate" component={VoyageCreate} />
        </Switch>
    </Router>, 
    document.getElementById('root')
);
