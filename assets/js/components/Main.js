import React, { Component } from 'react'
import Nav from './Nav'
import TravelsDisplay from './TravelsDisplay'
import Footer from './Footer'
import '../../styles/main.css'

class Main extends Component
{
	constructor(props) {
		super(props)

		if (localStorage.getItem('userLogged') !== 'true') {
			this.state = {
				userLogged: false
			}
		} else {
			this.state = {
				userLogged: true
			}
		}
	}

	handleDeconnection = () => {
        localStorage.setItem('userLogged', 'false')
		localStorage.setItem('token', "")
		this.setState({userLogged: false})
    }

    handleAdmin = () => {
		localStorage.setItem('admin', 'true')
	}

	render() {

		return (
			<div class="grid-container">
				<div class="nav-item">
					<Nav userLogged={this.state.userLogged} handleDeconnection={this.handleDeconnection} />
				</div>
				<div class="banner-item">
					<img src="https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fthumbs.dreamstime.com%2Fb%2Fvecteur-de-paysage-d-automne-30405625.jpg&f=1&nofb=1" alt="" width="100%"/>
				</div>
				<div class="travels-item">
					<TravelsDisplay />
				</div>
				<div class="footer-item">
					<Footer />
				</div>
			</div>
		)
	}
}

export default Main