import React, { Component } from 'react'
import '../../styles/travelOptions.css'

class TravelOptions extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        const optionsJsx = []
        
        this.props.options.map((option, index) => {
            optionsJsx.push(
                <div id={option.id} className="border m-2 option" onClick={this.props.handleOption}>
                    <p>Option {index + 1}: {option.titre}</p>
                    <p>{option.prix} euros</p>
                </div>
            )
        })

        return [optionsJsx]
    }
}

export default TravelOptions