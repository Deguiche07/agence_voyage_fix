import React,{ Component } from 'react';
import axios from 'axios';

import Nav from './Nav';
import Footer from './Footer';
import '../../styles/login.css'

class Login extends Component
{
    state = {
        email: '',
        password: ''
    };

    handleSubmit = (event) => {
        this.setState({
            email: event.target[0].value,
            password: event.target[1].value
        });

        event.preventDefault();

        // Requeste de log permettant de récupérer un token si email et password ok!
        axios({
            method: 'post',
            url: '/auth/login',
            params: {
                email: event.target[0].value,
                password: event.target[1].value
            }
        }).then(response => {

            if (response.data.message === "email or password is wrong."){
                alert("erreur email ou password wrong");

            } else {
            localStorage.setItem('token', response.data.token);
            localStorage.setItem('userLogged', 'true');
            }
        }).then(res => {
            axios({
                method: 'get',
                url: '/api',
                headers: {
                    'Authorization' : localStorage.token
                }
            }).then(response => {
                this.props.history.goBack();
            });
        }).catch(error => {
            console.log(error);
        });
    }

    render(){
        return (
            <>
                <Nav />

                <form onSubmit={this.handleSubmit} className="d-flex flex-wrap login-form">
                    <div className="inputs-item d-flex">
                        <div className="d-flex flex-wrap wrapper-field">
                            <label name="email" htmlFor="email" className="login-label">Email : </label>
                            <input type="text" id="email" name="email" placeholder="mail@mail.fr" className="login-input" />
                        </div>
                        <div className="d-flex flex-wrap wrapper-field">
                            <label name="password" htmlFor="password" className="login-label">Password : </label>
                            <input type="password" id="password" name="password" placeholder="****" className="login-input" />
                        </div>
                    </div>
                    <input type="submit" />
                </form>
            </>
        )
    }
}

export default Login;