import axios from 'axios';
import React, {Component} from 'react';
import Nav from './Nav';
import Footer from './Footer'
import '../../styles/signin.css'

class Signin extends Component
{
    state = {
        login: '',
        email: '',
        password: ''
    };

    handleSubmit = (event) => {
        this.setState({
            login: event.target[0].value,
            email: event.target[1].value,
            password: event.target[2].value
        });

        event.preventDefault();

        axios({
            method: 'post',
            url: '/auth/register',
            params: {
                login: event.target[0].value,
                email: event.target[1].value,
                password: event.target[2].value
            }
        });

        this.props.history.push('/home');       
    }

    render(){
        return(
            <div>

                <Nav/>

                <form onSubmit={this.handleSubmit} className="d-flex flex-wrap signin-form">
                    <div className="inputs-item d-flex">
                        <div className="d-flex flex-wrap wrapper-field">
                            <label name="login" htmlFor="login" className="signin-label">Login : </label>
                            <input type="text" id="login" name="login" placeholder="monPseudo" className="signin-input" />
                        </div>
                        <div className="d-flex flex-wrap wrapper-field">
                            <label name="email" htmlFor="email" className="signin-label">Email : </label>
                            <input type="text" id="email" name="email" placeholder="mail@mail.fr" className="signin-input" />
                        </div>
                        <div className="d-flex flex-wrap wrapper-field">
                            <label name="password" htmlFor="password" className="signin-label">Password : </label>
                            <input type="password" id="password" name="password" placeholder="***" className="signin-input"/>
                        </div>
                    </div>
                    <input type="submit" className="signin-button"/>                    
                </form>
            </div>
        )
    }
}

export default Signin;