import React, {Component} from 'react';
import Carousel from 'react-bootstrap/Carousel';
import '../../styles/carto.css'

const Carto = ({voyage}) => {

    const etapesJSX = voyage.etapes.map(etape => {

        const imagesNonTriees = [etape.photoUn, etape.photoDeux, etape.photoTrois]
        const imagesTriees = imagesNonTriees.filter(image => image !== null)

        const carouselItemJsx = imagesTriees.map(image => {
            return (
                <Carousel.Item>
                    <img
                        className="img-sized"
                        src={image}
                        alt="Image du voyage"
                    />
                </Carousel.Item>
            )
        })

        return (
            <>
                <h3>{etape.titre}</h3>
                <div className="d-flex flex-wrap">
                    <Carousel className="carto-carousel">
                        {carouselItemJsx}
                    </Carousel>
                    <div className="carto-etape">
                        <p>Num : {etape.numEtape}</p>
                        <p>Date : {etape.date.slice(0,10)}</p>
                    </div>
                    
                </div>
                <p>{etape.description}</p>
            </>
        )
    }) 

    return (
        <>
            <ol>
            {etapesJSX}
            </ol>
        </>
    )
}

export default Carto;