import React, { Component } from 'react'
import Travel from './Travel'
import '../../styles/travels.css'

const Travels = ({voyages}) => {

    const voyagesJsx = voyages.map((voyage, index) => {
        return (index + 1) % 2 === 0 ? 
            <Travel voyage={voyage} pairIndex={true} /> :
            <Travel voyage={voyage} pairIndex={false} />
    })

    return (
        <div className="d-flex flex-wrap">
            {voyagesJsx}
        </div>
    )
}

export default Travels