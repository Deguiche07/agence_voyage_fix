import React, {Component} from 'react';
import Nav from './Nav';

import {PayPalButton} from 'react-paypal-button-v2';

import axios from 'axios';

class Paiement extends Component
{
    constructor(props){
        super(props)

        this.amount = this.props.location.state.amount
        this.id_res = this.props.location.state.id_res
    }

    componentDidMount() {
        
    }

    render(){

        return (
            <>
            <Nav userLogged={true}/>
            <div>Paiement {this.amount}</div>
            <PayPalButton 
                amount = {this.amount}
                onSuccess={(details, data) => {

                    axios({
                        method: 'put',
                        url: 'api/reservers/'+this.id_res,
                        headers: {
                            'Content-Type': 'application/json',
                            'Authorization' : localStorage.token
                        },
                        data: {
                            paiementOk: true
                        }
                    }).then(res => {
                        alert("Transaction completed by " + details.payer.name.given_name);
                    }).then(res => {
                        this.props.history.goBack()
                    })
                }}
                options={{
                    clientId: "AceLjdDu_XTVLEhLBgr_H8T3Q05hkmqwgciV1wLggLJ18Do2DYx0TZ5VKDB3K4U75KFLkkgYDWV5NvZT",
                    currency: 'EUR'
                }}
            />
            </>
        );
    }
}

export default Paiement;