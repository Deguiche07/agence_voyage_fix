import React, { Component } from 'react'
import '../../styles/footer.css'

const Footer = () => {
    return (
        <div className="d-flex flex-wrap">
            <section className="partenaires border">
                <h2>Nos partenaires</h2>
                <div className="d-flex flex-wrap justify-content-between">
                    <img alt="" src="https://via.placeholder.com/150x75" className="img-sized part-item mb-2" />
                    <img alt="" src="https://via.placeholder.com/150x75" className="img-sized part-item mb-2" />
                    <img alt="" src="https://via.placeholder.com/150x75" className="img-sized part-item mb-2" />
                    <img alt="" src="https://via.placeholder.com/150x75" className="img-sized part-item mb-2" />
                </div>
            </section>
            <section className="contact border">
                <h2>Contact</h2>
                <p>Portable : 06.xx.xx.xx.xx</p>
                <p>Mail: mail@gmail.com</p>
            </section>
        </div>
    )
}

export default Footer