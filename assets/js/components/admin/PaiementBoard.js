import axios from 'axios';
import React, { Component } from 'react';
import NavAdmin from './NavAdmin';
import Button from 'react-bootstrap/Button'
import Table from 'react-bootstrap/Table';
import '../../../styles/paiementBoard.css';

class PaiementBoard extends Component
{

    constructor(props){
        super(props)

        this.state = {
            paiements: []
        }
    }

    getReservers(){
        axios({
            method: 'get',
            url: '/api/reservers.json',
            headers: {
                'Authorization': localStorage.token
            }
        }).then(response => {
            this.setState({
                paiements: response.data
            })
        }).catch(e => {
            console.log("error : ", e)
            if (e.response.status === 401) {
                this.props.history.push("/login")
            }
        })
    }

    componentDidMount(){
        this.getReservers();
    }

    toggleAutorisation(res){
        // Fonction pour l'autorisation de paiement
        
        const toggle = res.paiement_autorise ? false : true;

        axios({
            method: 'put',
            url: 'api/reservers/'+res.id,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': localStorage.token
            },
            data:
            {
                paiementAutorise: toggle
            }
        }).then(res => {
            
            this.getReservers()

        }).catch(e =>{
            console.log(e)
            if (e.response.status === 401) {
                this.props.history.push("/login")
            }
        })

        
    }

    render() {

        const paiements = this.state.paiements;

        let paiementsJSX = <h1> Chargement des réservations </h1>

        const tableHeaders = [
            'Voyage',
            'Login',
            'Options Souscrite',
            'Montant Total',
            'Date de demande',
            'Date de départ',
            'Place restante',
            'Paiement possible',
            'Paiement effectué'
        ]

        if (paiements.length === 0) {
        } else {

            paiementsJSX = paiements.map((paiement, index) => 
                <tr>
                    <td key={index}>{paiement.voyage.titre}</td>
                    <td key={index}>{paiement.user.login}</td>
                    <td key={index}>À implémenter</td>
                    <td key={index}>À implémenter</td>
                    <td key={index}>À implémenter</td>
                    <td key={index}>{paiement.voyage.dateDebut.slice(0,10)}</td>
                    <td key={index}>À implémenter</td>
                    <td onClick={this.toggleAutorisation.bind(this, paiement)}>
                        <Button>{String(paiement.paiement_autorise)}</Button>
                    </td>
                    <td>{String(paiement.paiement_ok)}</td>
                </tr>
                )

            paiementsJSX = (
                <div className="wrapper-paiementBoard">
                    <Table responsive>
                        <thead>
                            <tr>
                            {tableHeaders.map((header, index) => (
                                <th key={index}>{header}</th>
                            ))}
                            </tr>
                        </thead>
                        <tbody>
                            {paiementsJSX}
                        </tbody>
                    </Table>
                </div>
            )
        }

        return (
            <>
                <NavAdmin />
                
                {paiementsJSX}
            </>
        )
    }
}

export default PaiementBoard;