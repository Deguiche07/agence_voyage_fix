import React, { Component } from 'react';

import Nav from '../Nav';
import { Link } from 'react-router-dom';
import { Button } from 'react-bootstrap';
import '../../../styles/navAdmin.css';

class NavAdmin extends Component
{

    constructor(props){
        super(props)

		if (localStorage.getItem('userLogged') !== 'true') {
			this.state = {
				userLogged: false,
			}
		} else {
			this.state = {
				userLogged: true,
			}
		}
    }

    handleDeconnection = () => {
        localStorage.setItem('token', "")
        localStorage.setItem('userLogged', 'false');
    }

    render() {
        return (
            <>

                <Nav userLogged handleDeconnection={this.handleDeconnection}/>

                <ul>
                    <li>
                        <Link to={"/userboard"}>
                            User Board
                        </Link>  
                    </li>            
                    <li> 
                        <Link to={"/voyageboard"}>
                            Voyage Board
                        </Link>
                    </li> 
                    <li>
                        <Link to={"/paiementboard"}>
                            Paiement Board
                        </Link>
                    </li>
                </ul>
            </>
        )
    }

}

export default NavAdmin;