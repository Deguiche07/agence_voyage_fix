import axios from 'axios';
import React, { Component } from 'react';
import NavAdmin from './NavAdmin';
import { Link } from 'react-router-dom'
import Button from 'react-bootstrap/Button'
import Table from 'react-bootstrap/Table'
import '../../../styles/voyageBoard.css'

class VoyageBoard extends Component
{

    constructor(props){
        super(props)

        this.state = {
            voyages: []
        }
    }

    getPlacesRestantes= (voyage) => {
        let nbPlaceDispo = voyage.nbPlacesDisponibles;
        let nb_reserv_paid = 0;
        voyage.reservations.map((reserv) => { 
            if (reserv.paiement_ok){
                nb_reserv_paid++;
            }
        })
        return nbPlaceDispo - nb_reserv_paid
    }

    getPaiementsAttente= (voyage) => {
        let cpt = 0;
        voyage.reservations.map(reserv => {
            if (reserv.paiement_autorise & !reserv.paiement_ok){
                cpt++;
            }
        })
        return cpt;
    }

    deleteVoyage = (voyage, event) => {


        if (confirm("Êtes-vous sûr de vouloir supprimer ce voyage")) {
            axios({
                method: 'delete',
                url: 'api/voyages/'+voyage.id,
                headers: {
                    'Authorization': localStorage.token
                }
            }).then(res => {
                this.getAllVoyages();
            }).catch(e =>{
                if (e.response.status === 401) {
                    this.props.history.push("/login")
                }
            })
          } else {
            // Do nothing!
            alert("voyage non supprimé!");
          }

    }

    getAllVoyages= () => {
        axios({
            method: 'get',
            url: '/api/voyages.json',
            headers: {
                'Authorization': localStorage.token
            }
        }).then(response => {
            this.setState({
                voyages: response.data
            })

        }).then(response => {
            this.state.voyages.map(res => {
                res['placesRestantes'] = this.getPlacesRestantes(res)
                res['paiementAttente'] = this.getPaiementsAttente(res)
            })

        }).then(response => {
            this.setState({})
        }).catch(e => {
            console.log("error :", e)
            if (e.response.status === 401) {
                this.props.history.push("/login")
            }
        })
    }

    componentDidMount(){
        this.getAllVoyages()
    }

    render() {

        const voyages = this.state.voyages;

        let voyagesJSX = <h1> Chargement des voyages </h1>

        const tableHeaders = [
            'Voyage',
            'Options Disponibles',
            'nb Étapes',
            'Paiement en attente',
            'Date de Départ',
            'Place Disponibles',
            'Place restantes',
            'Détails',
            'Modifier',
            'Supprimer'
        ]
        
        if (voyages.length === 0) {
            voyagesJSX = <>
                            <h1> Pas de voyages trouvés! </h1>
                        </>
        } else {
            voyagesJSX = voyages.map((voyage, index) => 
                <tr key={index}>
                    <td >{voyage.titre}</td>
                    <td>{voyage.options.length}</td>
                    <td>{voyage.etapes.length}</td>
                    <td>{voyage.paiementAttente}</td>
                    <td>{voyage.dateDebut.slice(0,10)}</td>
                    <td>{voyage.nbPlacesDisponibles}</td>
                    <td>{voyage.placesRestantes}</td>
                    <td><Link to={{pathname:"/travel-details", state: {voyage: voyage}}}><Button>Détails!</Button></Link></td>
                    <td><Link to={{pathname:"/voyagecreate", state: {voyage: voyage}}}><Button>Modify!</Button></Link></td>
                    <td onClick={e => this.deleteVoyage(voyage, e)}>
                        <Button variant="danger">Supprimer</Button>
                    </td>
                </tr>
                )

            voyagesJSX = (
                <Table responsive>
                    <thead>
                        <tr>
                        {tableHeaders.map((header, index) => (
                            <th key={index}>{header}</th>
                        ))}
                        </tr>
                    </thead>
                    <tbody>
                        {voyagesJSX}
                    </tbody>
                </Table>
            )
        }

        return (

            <>
                <NavAdmin />

                <div className="wrapper-voyageBoard">
                    {voyagesJSX}

                    <Link to={"/voyagecreate"}>
                        <Button>Créer un voyage</Button>
                    </Link>
                </div>
            </>

        )
    }

}

export default VoyageBoard;