import React, { Component } from 'react'
import Nav from '../Nav'
import VoyageCreateForm from './form/VoyageCreateForm'
import EtapeCreateForm from './form/EtapeCreateForm'
import OptionCreateForm from './form/OptionCreateForm'

import ListeEtape from './ListeEtape'

import axios from 'axios'
import '../../../styles/voyageCreate.css'

class VoyageCreate extends Component
{

    constructor(props){
        super(props)

        this.handleSubmit = this.handleSubmit.bind(this);

        if (localStorage.getItem('userLogged') !== 'true') {
			this.state = {
				userLogged: false
			}
		} else {
			this.state = {
				userLogged: true
			}
		}

        if (props.location.state) {
            this.state = {
                etape_id : null,
                voyage : props.location.state.voyage,
                voyage_posted: true,
                voyage_id: props.location.state.voyage.id
            }

            this.state.voyage.dateDebut = this.state.voyage.dateDebut.slice(0,10);
            this.state.voyage.dateFin = this.state.voyage.dateFin.slice(0,10);

        } else {

            this.state = {
                etape_id : null,
                voyage_id : null,
                voyage_posted: false,
                voyage: {
                    titre: "Titre Voyage",
                    dateDebut: '2021-03-30',
                    dateFin: '2021-04-30',
                    nbPlacesDisponibles: 0,
                    prixInitial: 0, 
                    description: '',
                    photoUn: '',
                    photoDeux: '',
                    photoTrois: '',
                    photoQuatre: '',
                    lead: false,
                    etapes: [],
                    options: [],
                    reservers: []

                }
            }
        }
    }

    handleDeconnection = () => {
        localStorage.setItem('token', "")
        localStorage.setItem('userLogged', 'false');
        // props.history.push('/home')
    }

    handleSubmit = (event) => {

        event.preventDefault();
        this.setState({
            voyage: {
                titre: event.target[0].value,
                dateDebut: new Date(event.target[1].value).toISOString().slice(0,10),
                dateFin: new Date(event.target[2].value).toISOString().slice(0,10),
                nbPlacesDisponibles: event.target[3].value,
                prixInitial: event.target[4].value,
                description: event.target[5].value,
                photoUn: event.target[6].value,
                photoDeux: event.target[7].value,
                photoTrois: event.target[8].value,
                photoQuatre: event.target[9].value,
                lead: event.target[10].value
            }
        })

        if (this.state.voyage_posted) {
            this.putVoyage(event)
        } else {
            this.postVoyage(event)
        }


    }

    postVoyage(e) {
        axios({
            method: 'post',
            url: 'api/voyages',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': localStorage.token
            },
            data: {
                titre: e.target[0].value,
                dateDebut: new Date(e.target[1].valueAsNumber).toISOString().slice(0, 19).replace('T', ' '),
                dateFin: new Date(e.target[2].valueAsNumber).toISOString().slice(0, 19).replace('T', ' '),
                nbPlacesDisponibles: Number(e.target[3].value),
                prixInitial: Number(e.target[4].value),
                description: e.target[5].value,
                photoUn: e.target[6].value,
                photoDeux: e.target[7].value,
                photoTrois: e.target[8].value,
                photoQuatre: e.target[9].value,
                lead: Boolean(e.target[10].value),
                options: [],
                etapes: [],
                reservations: []
            }
        }).then(response => {
            this.setState({
                voyage_posted: true,
                voyage_id: response.data.id
            })
        }).catch(e => {
            if (e.response.status === 401) {
                this.props.history.push("/login")
            }
        })
    }

    putVoyage(e) {
        axios({
            method: 'put',
            url: 'api/voyages/'+this.state.voyage_id,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': localStorage.token
            },
            data: {
                titre: e.target[0].value,
                dateDebut: new Date(e.target[1].valueAsNumber).toISOString().slice(0, 19).replace('T', ' '),
                dateFin: new Date(e.target[2].valueAsNumber).toISOString().slice(0, 19).replace('T', ' '),
                nbPlacesDisponibles: Number(e.target[3].value),
                prixInitial: Number(e.target[4].value),
                description: e.target[5].value,
                photoUn: e.target[6].value,
                photoDeux: e.target[7].value,
                photoTrois: e.target[8].value,
                photoQuatre: e.target[9].value,
                lead: Boolean(e.target[10].value)            
            }
        }).catch(e => {
            if (e.response.status === 401) {
                this.props.history.push("/login")
            }
        })
    }

    render() {
        let relationJSX = <></>
        if (this.state.voyage_posted) {
            relationJSX = <>
                {/* <ListeEtape voyage={this.state.voyage}/> */}
                <EtapeCreateForm 
                    voyage_id={this.state.voyage_id}
                />
                <OptionCreateForm 
                    voyage_id={this.state.voyage_id}
                />
            </>
        }
        
        return (
            <div>
                <Nav userLogged handleDeconnection={this.handleDeconnection}/>

                <div className="d-flex wrapper-voyageCreation">
                    <div className="details-voyageCreation">
                        <VoyageCreateForm 
                            voyage_posted={this.state.voyage_posted}
                            handleSubmit={this.handleSubmit}
                            voyage={this.state.voyage}
                        />
                    </div>
                    <div className="d-flex flex-wrap relations-voyageCreation">
                        {relationJSX}
                    </div>
                </div>
            </div>

        )
    }
}

export default VoyageCreate