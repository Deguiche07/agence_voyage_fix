import React, { Component } from 'react';
import NavAdmin from './NavAdmin';
import axios from 'axios';
import Table from 'react-bootstrap/Table'
import '../../../styles/userBoard.css'
import { Link } from 'react-router-dom';
import { Button } from 'react-bootstrap';

class UserBoard extends Component
{

    constructor(props){
        super(props)

        this.state = {
            users: []
        }
    }

    getUsers= () => {
        axios({
            method: 'get',
            url: '/api/users.json',
            headers: {
                'Authorization': localStorage.token
            }
        }).then(response => {
            this.setState({
                users: response.data
            });
        }).then(res => {
            this.state.users.map(user => {
                user['paiementAttente'] = this.getPaiementAttente(user);
            })

            this.setState({
                users : this.state.users
            })
        })
        .catch(e => {
            console.log('Erreur :',e)
            if (e.response.status === 401) {
                this.props.history.push("/login")
            }
        })
    }

    deleteUser= (user, event) => {
        if (confirm("Êtes-vous sûr de vouloir supprimer cet Utilisateur ?")) {
            axios({
                method: 'delete',
                url: 'api/users/'+user.id,
                headers: {
                    'Authorization': localStorage.token
                }
            }).then(res => {
                this.getUsers();
            })
        } else {
            alert("user non supprimé")
        }
    }

    getPaiementAttente(user) {
        let cpt = 0
        user.reservations.map(reserv => {
            if (reserv.paiement_autorise & !reserv.paiement_ok) {
                cpt++;
            }
        })
        return cpt;
    }

    componentDidMount(){
        this.getUsers();
    }

    render() {

        const users = this.state.users;

        let userJSX = <h1> En attente de chargement</h1>;

        const tableHeaders = [
            'Supprimer',
            'Login',
            'Email',
            'Nombre Voyages réservé',
            'Paiement en attente',
            'Nombre voyage effectué',
            'Détails'
        ]

        if (users.length === 0) {
        } else {
            userJSX = users.map((user, index) => 
                <tr key={index}>
                    <td  onClick={e =>this.deleteUser(user,e)}>
                        <Button variant="danger">Supprimer</Button>
                    </td>
                    <td>{user.login}</td>
                    <td>{user.email}</td>
                    <td>{user.reservations.length}</td>
                    <td>{user.paiementAttente}</td>
                    <td>A implementer</td>
                    <td><Link to={{pathname:"/profile", state: {userLogged:true, user_id:user.id}}}><Button>Détails!</Button></Link></td>
                </tr>
            )

            userJSX = (
                <div className="wrapper-userBoard">
                    <Table responsive>
                        <thead>
                            <tr>
                            {tableHeaders.map((header, index) => (
                                <th key={index}>{header}</th>
                            ))}
                            </tr>
                        </thead>
                        <tbody>
                            {userJSX}
                        </tbody>
                    </Table>
                </div>
            )
        }

        return (
            <>
                <NavAdmin/>

                {userJSX}
            </>
        )
    }
}

export default UserBoard;