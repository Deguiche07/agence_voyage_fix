import React, { Component } from 'react'
import axios from 'axios'
import '../../../../styles/optionCreateForm.css'

class OptionCreateForm extends Component {

    constructor(props){
        super(props)

        this.state = {
            option: {
                titre: "",
                description: "",
                dateDebut: "2021-03-31",
                dateFin: "2021-04-20",
                prix: 0,
                reservers: []
            }
        }
    }

    handleSubmit = (e) => {

        e.preventDefault()

        axios({
            method: "post",
            url: 'api/options',
            headers: {
                "Authorization": localStorage.token,
                "Content-Type": 'application/json'
            },
            data: {
                titre: e.target[0].value,
                dateDebut: new Date(e.target[1].valueAsNumber).toISOString().slice(0,19).replace('T', ' '),
                dateFin: new Date(e.target[2].valueAsNumber).toISOString().slice(0,19).replace('T', ' '),
                prix: Number(e.target[3].value),
                description: e.target[4].value,
                reservers: [],
                voyages: ['api/voyages/'+this.props.voyage_id]
            }
        }).catch(e => {
            console.log(e)
        })
    }

    render () {
        return (
            <div className="d-flex wrapper-form-option-create">
                <h2 className="title-mb-2">Ajouter une option au voyage</h2>
                <form className="form-option-create" onSubmit={this.handleSubmit}>
                    <div className="d-flex flex-wrap wrapper-field-option-create">
                        <label name="titre" htmlFor="titre" className="option-create-form-label">Titre : </label>
                        <input type="text" 
                                id="titreOption" 
                                name="titre" 
                                placeholder="l'andalousie de jour"
                                className="option-create-form-input"
                        />
                    </div>
                    <div className="d-flex flex-wrap wrapper-field-option-create">
                        <label name="dateDebutOption" htmlFor="dateDebutOption" className="option-create-form-label">Date de début : </label>
                        <input type="date" 
                                id="dateDebutOption" 
                                name="dateDebutOption"
                                className="option-create-form-input"
                        />
                    </div>
                    <div className="d-flex flex-wrap wrapper-field-option-create">
                        <label name="dateFinOption" htmlFor="dateFinOption" className="option-create-form-label">Date de fin : </label>
                        <input type="date" 
                                id="dateFinOption" 
                                name="dateFinOption"
                                className="option-create-form-input"
                        />
                    </div>
                    <div className="d-flex flex-wrap wrapper-field-option-create">
                        <label name="prixOption" htmlFor="prixOption" className="option-create-form-label">Prix : </label>
                        <input type="number" 
                                id="prixOption" 
                                name="prixOption" 
                                className="option-create-form-input"
                        />
                    </div>
                    <div className="d-flex flex-wrap wrapper-field-option-create">
                        <label name="textareaOption" htmlFor="textareaOption" className="option-create-form-label">description : </label>
                        <textarea id="textareaOption" 
                            name="textareaOption" 
                            rows="5" 
                            // cols="33" 
                            placeholder="Entrez la description de votre voyage..."
                            className="option-create-form-input"
                        />
                    </div>
                    <div>
                        <input type="submit" value="Enregistrer Option" className="submit-option-create" />
                    </div>
                </form>
            </div>
        )
    }
}

export default OptionCreateForm