import React, { Component } from 'react'
import '../../../../styles/voyageCreateForm.css'

class VoyageCreateForm extends Component {

    constructor(props){
        super(props);
        
        this.state = {
            voyage: props.voyage
        }
    }

    render () {

        return (
            <>
                <h2 className="title-mb-2">Créer un nouveau voyage</h2>
                <form className="form-voyage-create" onSubmit={this.props.handleSubmit}>
                    <div className="d-flex flex-wrap wrapper-inputs-form-voyage-create">
                        <div className="form-voyage-create-left">
                            <div className="d-flex flex-wrap wraper-field-form-voyage-create">
                                <label name="titre" htmlFor="titre" className="voyage-create-form-label">Titre : </label>
                                <input type="text" 
                                        id="titreVoyage" 
                                        name="titre" 
                                        value={this.state.voyage.titre}
                                        onChange={e => this.setState({ voyage: {titre : e.target.value} })}
                                        className="voyage-create-form-input"
                                />
                            </div>
                            <div className="d-flex flex-wrap wraper-field-form-voyage-create">
                                <label name="dateDebut" htmlFor="dateDebut" className="voyage-create-form-label">Date de début : </label>
                                <input type="date" 
                                        id="dateDebutVoyage" 
                                        name="dateDebut" 
                                        value={this.state.voyage.dateDebut}
                                        onChange={e => this.setState({voyage: {dateDebut: e.target.value} })}
                                        className="voyage-create-form-input"
                                />
                            </div>
                            <div className="d-flex flex-wrap wraper-field-form-voyage-create">
                                <label name="dateFin" htmlFor="dateFin" className="voyage-create-form-label">Date de fin : </label>
                                <input type="date" 
                                        id="dateFinVoyage" 
                                        name="dateFin" 
                                        value={this.state.voyage.dateFin}
                                        onChange={e => this.setState({voyage: {dateFin: e.target.value} })}
                                        className="voyage-create-form-input"
                                />
                            </div>
                            <div className="d-flex flex-wrap wraper-field-form-voyage-create">
                                <label name="nbPlaces" htmlFor="nbPlaces" className="voyage-create-form-label">Nombre de places : </label>
                                <input type="number" 
                                        id="nbPlacesDisponibles" 
                                        name="nbPlaces" 
                                        value={this.state.voyage.nbPlacesDisponibles}
                                        onChange={e => this.setState({voyage: {nbPlacesDisponibles: e.target.value} })}
                                        className="voyage-create-form-input"
                                />
                            </div>
                            <div className="d-flex flex-wrap wraper-field-form-voyage-create">
                                <label name="prixInitial" htmlFor="prixInitial" className="voyage-create-form-label">Prix intial : </label>
                                <input type="number" 
                                        id="prixInitial" 
                                        name="prixInitial" 
                                        value={this.state.voyage.prixInitial}
                                        onChange={e => this.setState({voyage: {prixInitial: e.target.value}})}
                                        className="voyage-create-form-input"
                                />
                            </div>
                            <div className="d-flex flex-wrap wraper-field-form-voyage-create">
                                <label name="textareaVoyage" htmlFor="textareaVoyage" className="voyage-create-form-label">description : </label>
                                <textarea id="textareaVoyage" 
                                            name="textareaVoyage" 
                                            rows="5" 
                                            // cols="33"
                                            value={this.state.voyage.description}
                                            onChange={e => this.setState({voyage: {description: e.target.value}})}
                                            className="voyage-create-form-input"
                                />
                            </div>
                        </div>
                        <div className="form-voyage-create-right">
                            <div className="d-flex flex-wrap wraper-field-form-voyage-create">
                                <label name="photoUn" htmlFor="photoUn" className="voyage-create-form-label">Photo 1 : </label>
                                <input type="text" 
                                        id="photoUn" 
                                        name="photoUn" 
                                        value={this.state.voyage.photoUn}
                                        onChange={e => this.setState({voyage: {photoUn: e.target.value}})}
                                        className="voyage-create-form-input"
                                />
                            </div>
                            <div className="d-flex flex-wrap wraper-field-form-voyage-create">
                                <label name="photoDeux" htmlFor="photoDeux" className="voyage-create-form-label">Photo 1 : </label>
                                <input type="text" 
                                        id="photoDeux" 
                                        name="photoDeux" 
                                        value={this.state.voyage.photoDeux}
                                        onChange={e => this.setState({voyage: {photoDeux: e.target.value}})}
                                        className="voyage-create-form-input"
                                />
                            </div>
                            <div className="d-flex flex-wrap wraper-field-form-voyage-create">
                                <label name="photoTrois" htmlFor="photoTrois" className="voyage-create-form-label">Photo 1 : </label>
                                <input type="text" 
                                        id="photoTrois" 
                                        name="photoTrois" 
                                        value={this.state.voyage.photoTrois}
                                        onChange={e => this.setState({voyage: {photoTrois: e.target.value}})}
                                        className="voyage-create-form-input"
                                />
                            </div>
                            <div className="d-flex flex-wrap wraper-field-form-voyage-create">
                                <label name="photoQuatre" htmlFor="photoQuatre" className="voyage-create-form-label">Photo 1 : </label>
                                <input type="text" 
                                        id="photoQuatre" 
                                        name="photoQuatre" 
                                        value={this.state.voyage.photoQuatre}
                                        onChange={e => this.setState({voyage: {photoQuatre: e.target.value}})}
                                        className="voyage-create-form-input"
                                />
                            </div>
                            <div className="wraper-field-form-voyage-create">
                                <label name="lead" htmlFor="lead" className="voyage-create-form-label">Voyage à mettre en avant : </label>
                                <input type="checkbox" 
                                        id="lead" 
                                        name="lead" 
                                        value={this.state.voyage.lead}
                                        onChange={e => this.setState({voyage: {lead: e.target.checked}})}
                                        className="voyage-create-form-input"
                                />
                            </div>
                        </div>  
                    </div>
                    <input type="submit" value="enregistrer le voyage" className="submit-voyage-creation" />
                </form>
            </>

        )
    }
}

export default VoyageCreateForm