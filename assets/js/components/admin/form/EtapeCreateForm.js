import React, { Component } from 'react'
import '../../../../styles/etapeCreateForm.css'

import axios from 'axios'

class EtapeCreateForm extends Component {

    constructor(props){
        super(props)

        this.etape_id = this.props.etape_id ? this.props.etape_id : null

        this.state = {
                etape_posted : false,
                etape : {
                    voyage: props.voyage_id,
                    titre: "",
                    date: "2021-03-31",
                    numEtape: "0",
                    description: "",
                    photoUn: "",
                    photoDeux: "",
                    photoTrois: ""
                }
            }
    
        }
    
    componentDidMount() {
        this.getEtape()
    }

    getEtape() {
        if (this.etape_id){

            axios({
                method: 'get',
                url: 'api/etapes/'+this.etape_id,
                headers: {
                    'Authorization': localStorage.token
                }
            }).then(res => {

                res.data.date = res.data.date.slice(0,10)

                this.setState({
                    etape_posted : true,
                    etape: res.data
                })
            })
        }
    }

    putEtape= (id, e) => {
        axios({
            method: 'put',
            url: 'api/etapes/'+this.etape_id,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': localStorage.token
            },
            data: {
                voyage: 'api/voyages/'+this.props.voyage_id,
                titre: e.target[0].value,
                date: new Date(e.target[1].valueAsNumber).toISOString().slice(0, 19).replace('T', ' '),
                numEtape: Number(e.target[2].value),
                description: e.target[3].value,
                photoUn: e.target[4].value,
                photoDeux: e.target[5].value,
                photoTrois: e.target[6].value            
            }
        })
    }

    postEtape= (e) => {
        axios({
            method: 'post',
            url: 'api/etapes',
            header: {
                'Content-Type': 'application/json',
                'Authorization': localStorage.token
            },
            data: {
                voyage: 'api/voyages/'+this.props.voyage_id,
                titre: e.target[0].value,
                date: new Date(e.target[1].valueAsNumber).toISOString().slice(0, 19).replace('T', ' '),
                numEtape: Number(e.target[2].value),
                description: e.target[3].value,
                photoUn: e.target[4].value,
                photoDeux: e.target[5].value,
                photoTrois: e.target[6].value
            }
        }).then(response => {

            response.data.date = response.data.date.slice(0, 10)

            this.etape_id = response.data.id

            this.setState({
                etape_posted: true,
                etape: response.data
            })
        }).then(res => {
            this.setState({})
        }).catch(e => {
            console.log(e)
        })
    }

    handleSubmit = (e) => {

        e.preventDefault()

        if (this.state.etape_posted) {

            this.putEtape(this.etape_id, e)

        } else {

            this.postEtape(e);

        }

        this.getEtape();

    }

    render () {
        return (
            <div className="d-flex wrapper-form-etape-create">
                <h2 className="title-mb-2">Ajouter une étape au voyage</h2>
                <form className="d-flex form-etape-create" onSubmit={this.handleSubmit}>
                    <div className="d-flex flex-wrap wrapper-field-etape-create">
                        <label name="titre" htmlFor="titre" className="etape-create-form-label">Titre : </label>
                        <input type="text" 
                                id="titreEtape" 
                                name="titre" 
                                value={this.state.etape.titre}
                                onChange={e =>this.setState({ etape :{titre: e.target.value}})}
                                className="etape-create-form-input"
                        />
                    </div>
                    <div className="d-flex flex-wrap wrapper-field-etape-create">
                        <label name="dateEtape" htmlFor="dateEtape" className="etape-create-form-label">Date de l étape' : </label>
                        <input type="date" 
                                id="dateEtape" 
                                name="dateEtape" 
                                value={this.state.etape.date}
                                onChange={e =>this.setState({ etape : {date: e.target.value }})}
                                className="etape-create-form-input"
                        />
                    </div>
                    <div className="d-flex flex-wrap wrapper-field-etape-create">
                        <label name="numEtape" htmlFor="numEtape" className="etape-create-form-label">numéro de l étape : </label>
                        <input type="number" 
                                id="numEtape" 
                                name="numEtape" 
                                value={this.state.etape.numEtape}
                                onChange={e => this.setState({etape : {numEtape: e.target.value}})}
                                className="etape-create-form-input"
                        />
                    </div>
                    <div className="d-flex flex-wrap wrapper-field-etape-create">
                        <label name="textareaEtape" htmlFor="textareaEtape" className="etape-create-form-label">description : </label>
                        <textarea id="textareaEtape" 
                                    name="textareaEtape" 
                                    rows="5" 
                                    // cols="33" 
                                    value={this.state.etape.description}
                                    onChange={e => this.setState({etape: { description: e.target.value}})}
                                    className="etape-create-form-input"
                        />
                    </div>
                    <div className="d-flex flex-wrap wrapper-field-etape-create">
                        <label name="photoUn" htmlFor="photoUn" className="etape-create-form-label">Photo 1 : </label>
                        <input type="text" 
                                id="photoUn" 
                                name="photoUn" 
                                value={this.state.etape.photoUn}
                                onChange={e => this.setState({etape: {photoUn: e.target.value}})}
                                className="etape-create-form-input"
                        />
                    </div>
                    <div className="d-flex flex-wrap wrapper-field-etape-create">
                        <label name="photoDeux" htmlFor="photoDeux" className="etape-create-form-label">Photo 1 : </label>
                        <input type="text" 
                                id="photoDeux" 
                                name="photoDeux" 
                                value={this.state.etape.photoDeux}
                                onChange={e => this.setState({etape: {photoDeux: e.target.value}})}
                                className="etape-create-form-input"
                        />
                    </div>
                    <div className="d-flex flex-wrap wrapper-field-etape-create">
                        <label name="photoTrois" htmlFor="photoTrois" className="etape-create-form-label">Photo 1 : </label>
                        <input type="text" 
                                id="photoTrois" 
                                name="photoTrois" 
                                value={this.state.etape.photoTrois}
                                onChange={e => this.setState({etape: {photoTrois: e.target.value}})}
                                className="etape-create-form-input"
                        />
                    </div>
                    <div className="">
                        {/* <Link className={"nav-link w-50"} to={"/EtapeCreateForm"}>
                            <Button variant="outline-warning">enregistrer les étapes</Button>
                        </Link> */}
                        <input type="submit" value="Enregistrer l'Étape" className="submit-etape-create" />
                    </div>

                </form>
            </div>
        )
    }
}

export default EtapeCreateForm
