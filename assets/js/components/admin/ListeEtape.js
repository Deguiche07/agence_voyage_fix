import React, {Component} from 'react';

class ListeEtape extends Component
{
    constructor(props){
        super(props)

        this.state = {
            voyage: props.voyage
        }
    }

    render(){

        const listeEtapeJSX = this.state.voyage.etapes.map(etape => 
            <p>{etape.titre}</p>
        )

        return(
            <>
                <div>
                    <p>Nouvelle Étape</p>
                    {listeEtapeJSX}</div>
            </>
        )
    }

}

export default ListeEtape;