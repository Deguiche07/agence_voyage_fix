import React, { Component } from 'react'
import {Link} from 'react-router-dom'
import '../../styles/travel.css'

const Travel = ({voyage, pairIndex}) => {

    const appendLeadingZeroes = n => {
        return n <= 9 ? "0" + n : n
    }

    const dateDebut = new Date(voyage.dateDebut)
    const dateDebutLisible = dateDebut.getDate() + "/" + appendLeadingZeroes(dateDebut.getMonth()+1) + "/" + dateDebut.getFullYear()

    const dateFin = new Date(voyage.dateFin)
    const dateFinLisible = dateFin.getDate() + "/" + appendLeadingZeroes(dateFin.getMonth()+1) + "/" + dateFin.getFullYear()

    let travelJsx = pairIndex === true ? (
        <div className="d-flex border pair mbottom-2">
            <div className="wrapper-img">
                <img alt="" src={voyage.photoUn} className="img-sized" />
            </div>
            <div className="d-flex wrapper-wrapper-data">
                <div className="d-flex wrapper-data">
                    <div className="item">{voyage.etapes.length} étapes</div>
                    <div className="item">{voyage.nbPlacesDisponibles} places disponibles</div>
                    <div className="item">{voyage.prixInitial} euros</div>
                    <div className="item">Du {dateDebutLisible} au {dateFinLisible}</div>
                </div>
            </div>
        </div>
    ) :
    (
        <div className="d-flex border odd mbottom-2">
            <div className="wrapper-img">
                <img alt="" src={voyage.photoUn} className="img-sized" />
            </div>
            <div className="d-flex wrapper-wrapper-data">
                <div className="d-flex wrapper-data">
                    <div className="item">{voyage.etapes.length} étapes</div>
                    <div className="item">{voyage.nbPlacesDisponibles} places disponibles</div>
                    <div className="item">{voyage.prixInitial} euros</div>
                    <div className="item">Du {dateDebutLisible} au {dateFinLisible}</div>
                </div>
            </div>
        </div>
    )


    return (
        <Link className={"nav-link"} to={{
            pathname: "/travel-details",
            state: {
                voyage: voyage
            }
        }}>
            {travelJsx}
        </Link>
    )
}

export default Travel

// return (
//     <Col md={6}>
//         <Link className={"nav-link"} to={{
//             pathname: "/travel-details",
//             state: {
//                 voyage: voyage
//             }
//         }}>
//             <div className="d-flex border">
//                 <img alt="" src={voyage.photoUn} className="img-voyage" style={style} />
//                 <div className="d-flex flex-column">
//                     <div className="d-flex justify-content-around">
//                         <div>{voyage.etapes.length} étapes</div>
//                         <div>{voyage.nbPlacesDisponibles} places disponibles</div>
//                         <div>{voyage.prixInitial} euros</div>
//                     </div>
//                     <div>Du {dateDebutLisible} au {dateFinLisible} </div>
//                 </div>
//             </div>
//         </Link>
//     </Col>
// )