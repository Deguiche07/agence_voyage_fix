import React, { Component } from 'react'
import {Link} from 'react-router-dom'
import '../../styles/travelLead.css'

const TravelLead = ({voyage}) => {

    const appendLeadingZeroes = n => {
        return n <= 9 ? "0" + n : n
    }

    const dateDebut = new Date(voyage.dateDebut)
    const dateDebutLisible = dateDebut.getDate() + "/" + appendLeadingZeroes(dateDebut.getMonth()) + "/" + dateDebut.getFullYear()

    const dateFin = new Date(voyage.dateFin)
    const dateFinLisible = dateFin.getDate() + "/" + appendLeadingZeroes(dateFin.getMonth()) + "/" + dateFin.getFullYear()

    return (
        <Link className={"nav-link p-0"} to={{
            pathname: "/travel-details",
            state: {
                voyage: voyage
            }
        }}>
            <div className="d-flex flex-wrap border">
                <div className="wrapper-img-lead">
                    <img alt="" src={voyage.photoUn} className="img-sized" />
                </div>
                <div className="d-flex wrapper-wrapper-data">
                    <div className="d-flex wrapper-data wrapper-data-lead">
                        <div className="item">{voyage.etapes.length} étapes</div>
                        <div className="item">{voyage.nbPlacesDisponibles} places disponibles</div>
                        <div className="item">{voyage.prixInitial} euros</div>
                        <div className="item">Du {dateDebutLisible} au {dateFinLisible}</div>
                    </div>
                </div>
            </div>
        </Link>
    )
}

export default TravelLead