import React, { Component } from 'react'
import Carousel from 'react-bootstrap/Carousel'

class TravelCarousel extends Component {
    constructor(props) {
        super(props)

        this.voyage = props.voyage
    }

    render() {
        const imagesNonTriees = [this.voyage.photoUn, this.voyage.photoDeux, this.voyage.photoTrois, this.voyage.photoQuatre]
        const imagesTriees = imagesNonTriees.filter(image => image !== null)

        const carouselItemJsx = imagesTriees.map(image => {
            return (
                <Carousel.Item>
                    <img
                        className="d-block w-100"
                        src={image}
                        alt="Image du voyage"
                    />
                </Carousel.Item>
            )
        })

        return (
            <Carousel>
                {carouselItemJsx}
            </Carousel>
        )
    }
}

export default TravelCarousel