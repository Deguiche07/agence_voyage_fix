import React, { Component, useEffect, useState } from 'react'
import TravelLead from './TravelLead'
import Travels from './Travels'
import axios from 'axios'

const TravelsDisplay = () => {

    const [jsx, setJsx] = useState(null);

    useEffect(() => {
        axios({
            method: 'get',
            url: '/api/voyages.json'
        })
        .then(response => {

            if (response.data.length === 0) {
                setJsx( <p>Pas de voyage en Base de Donnée</p>)
            } else {

                let voyageLead = response.data[0]
                response.data.splice(0, 1)
                const voyages = response.data
                let voyagesDisponibles = []
                let anciensVoyages = []

                voyages.map(voyage => {
                    Date.now() < new Date(voyage.dateDebut) ? voyagesDisponibles.push(voyage) : anciensVoyages.push(voyage)
                })

                setJsx(
                    <>
                        <h2>Voyages disponibles à la réservation</h2>
                        <div>
                            <h3>Prochain voyage</h3>
                            <TravelLead voyage={voyageLead} />
                        </div>
                        <div>
                            <h3>Voyages à venir</h3>
                            <Travels voyages={voyagesDisponibles} />
                        </div>
                        <div>
                            <h2>Anciens voyages</h2>
                            <Travels voyages={anciensVoyages} />
                        </div>
                    </>
                )
            }
        })
        .catch(error => {
            console.log('Error', error)
        })
    }, [])

    
    if (jsx) {
        return jsx
    } else {
        return <p>Aucun voyage</p>
    }
}

export default TravelsDisplay