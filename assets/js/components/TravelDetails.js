import React, { Component } from 'react'
import Button from 'react-bootstrap/Button'
import Nav from './Nav'
import TravelCarousel from './TravelCarousel'
import TravelOptions from './TravelOptions'
import Footer from './Footer'
import Carto from './Carto'

import axios from 'axios'

import {Link} from 'react-router-dom';
import jwt_decode from 'jwt-decode';
import { PayPalButton } from 'react-paypal-button-v2'
import '../../styles/travelDetails.css'

class TravelDetails extends Component {
    constructor(props) {
        super(props)
        this.voyage = this.props.location.state.voyage

        this.optionsSelectionStatus = {}
        this.voyage.options.map(option => {
            this.optionsSelectionStatus[option.id] = false
        })
        
        this.state = {
            optionsSelectionStatus: this.optionsSelectionStatus,
            prixTotal: this.voyage.prixInitial,
            userLogged: localStorage.getItem('userLogged') === 'true' ? true : false
        } 
    }

    handleOption = (e) => {
        const optionWrapperElement = e.target.nodeName === "P" ? e.target.parentNode : e.target

        const optionId = optionWrapperElement.id
        const optionPrixTextNode = optionWrapperElement.childNodes[1].textContent
        const optionPrixFromPattern = optionPrixTextNode.match(/\d+/)

        if(this.state.optionsSelectionStatus[optionId] === false) {
            optionWrapperElement.classList.add('selected')

            this.optionsSelectionStatus[optionId] = true

            this.setState({
                prixTotal: this.state.prixTotal + Number(optionPrixFromPattern[0]),
                optionsSelectionStatus: this.optionsSelectionStatus
            })
        } else {
            optionWrapperElement.classList.remove('selected')

            this.optionsSelectionStatus[optionId] = false

            this.setState({
                prixTotal: this.state.prixTotal - Number(optionPrixFromPattern[0]),
                optionsSelectionStatus: this.optionsSelectionStatus
            })
        }
    }

    handleDeconnection = () => {
        localStorage.setItem('token', "")
        localStorage.setItem('userLogged', 'false')
		this.setState({userLogged: false})
    }

    handleConfirmReservation = () => {
        const jwt_decoded = jwt_decode(localStorage.token);
        const user_id = jwt_decoded["id"]
        const voyage_id = this.voyage.id

        const option_list = []

        Object.keys(this.optionsSelectionStatus).map(res => {
            if (this.optionsSelectionStatus[res]) {
            option_list.push("api/options/"+res)
            }
        })

        axios({
            method: "post",
            url: 'api/reservers',
            headers : {
                'Content-Type': 'application/json',
                'Authorization' : localStorage.token
            },
            data:
                {   
                    paiementAutorise: false,
                    paiementOk: false,
                    voyage: '/api/voyages/'+voyage_id,
                    user: '/api/users/'+user_id,
                    options: option_list
                }
        }).then(res => {
            console.log(res)
        })
    }

    getPlacesRestante= (voyage) => {
        let nbPlaceDispo = voyage.nbPlacesDisponibles;
        let nb_reserv_paid = 0;
        voyage.reservations.map((reserv) => { 
            if (reserv.paiement_ok){
                nb_reserv_paid++;
            }
        })
        return nbPlaceDispo - nb_reserv_paid
    }

    render() {
        const jsx = this.state.userLogged ?
            <div className="d-flex flex-wrap align-items-center">
                <p className="mb-0 mr-2">Prix total: {this.state.prixTotal} euros</p>
                <Button variant="outline-primary" onClick={this.handleConfirmReservation}>Confirmer la demande de réservation</Button>
            </div>
            :
            <div className="d-flex flex-wrap align-items-center">
                <p className="mb-0 mr-2">Place(s) disponible(s) : {this.getPlacesRestante(this.voyage)}</p>
                <Link className={"nav-link"} to={"/login"}> 
                    <Button variant="outline-primary">Réserver</Button>
                </Link>
            </div>

        const handleOptionJsx = this.state.userLogged ?
            <TravelOptions options={this.voyage.options} userLogged={this.userLogged} handleOption={this.handleOption} />:
            <TravelOptions options={this.voyage.options} userLogged={this.userLogged} />

        return (
            <>
                <Nav userLogged={this.state.userLogged} handleDeconnection={this.handleDeconnection} />

                <div className="d-flex flex-column wrapper-details">
                    <div>
                        <h1>Voyage : {this.voyage.titre}</h1>
                        <div className="d-flex flex-wrap" >
                            <TravelCarousel voyage={this.voyage} className="travel-carousel" />
                            <div className="description">
                                <h2>Description</h2>
                                <p>{this.voyage.description}</p>
                            </div>
                        </div>
                    </div>
                    <Carto voyage={this.voyage} className="carto"/>
                    <div>
                        <h2>Options proposées</h2>
                        <div className="d-flex flex-wrap">
                            {handleOptionJsx}
                        </div>
                    </div>
                    <div>
                        <h2>Réserver</h2>
                        {jsx}
                    </div>
                </div>

                <Footer />
            </>
        )
    }
}

export default TravelDetails