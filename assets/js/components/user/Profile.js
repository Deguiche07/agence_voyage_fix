import React, { Component } from 'react'
import {Link} from 'react-router-dom'
import Nav from '../Nav'
import Footer from '../Footer'

import jwt_decode from 'jwt-decode'
import axios from 'axios'
import Table from 'react-bootstrap/Table'
import '../../../styles/profile.css'

import {PayPalButton} from 'react-paypal-button-v2';
import { Button } from 'react-bootstrap'

class Profile extends Component
{
    constructor(props){
        super(props)

		if (localStorage.getItem('userLogged') !== 'true') {
			this.state = {
				userLogged: false,
                user : {
                },
                user_id: props.location.state.user_id
			}
		} else {
			this.state = {
				userLogged: true,
                user: {
                },
                user_id: props.location.state.user_id
			}
		}
    }

    handleDeconnection= () => {
        localStorage.setItem('token', "")
        localStorage.setItem('userLogged', 'false');
        // props.history.push('/home')
    }

    montantTotal(reserv) {
        let somme = 0

        reserv['options'].map(opt => {
            somme += opt.prix
        })

        return somme + reserv.voyage.prixInitial
    }

    componentDidMount(){
        axios({
            method: 'get',
            url: '/api/users/'+this.state.user_id,

        }).then(res => {
            
            const montantTotal = res.data["reservations"].map(res => {
             
                res['montantTotal'] = this.montantTotal(res)
                if (res.paiement_ok) {
                    res['button'] = <><Button disabled>Déjà payé!</Button></>
                } else {
                    if (res.paiement_autorise) {
                        res['button'] = <><Button variant="success">Payer!</Button></>
                    } else {
                    res['button'] = <><Button variant="warning" disabled>Attente Autorisation</Button></>
                    }
                }

            })
            
            this.setState({
                user: res.data
            })

        }).then(res => {

            const user = this.state.user

            user['reservations'].map(reservation => {
                reservation['optionsSouscrites'] = this.getOptionsSouscrites(reservation)
                axios({
                    method:'get',
                    url: 'api/voyages/'+reservation.voyage.id,
                    headers: {'Authorization': localStorage.token}
                }).then(response => {
                    let nbPlaceVoyage = response.data.nbPlacesDisponibles;
                    let list_reserv = response.data.reservations
                    let cpt = 0
                    list_reserv.map(reserv => {
                        if (reserv.paiement_ok){
                            cpt++;
                        }
                    })
                    reservation['nbPlacesRestante'] = nbPlaceVoyage - cpt
                }).then(res => {
                    this.setState({
                        user: user
                    })
                }).catch(e => {
                    console.log(e)
                    if (e.response.status === 401) {
                        this.props.history.push("/login")
                    }
                })
            })
        })
    }

    getOptionsSouscrites = (res) => {
        if (Object.keys(res.options).length === 0) {
            return <>Pas D'options souscrites</>
        } else {
            let list_titre_option = res.options.map(opt => 
                <li>{opt.titre}</li>
            );
            return <><ol>{list_titre_option}</ol></>
        }
    }

    render() {
        const user = this.state.user;
        
        let reservationsJSX = []
        if (Object.keys(user).length===0){
        } else {

            reservationsJSX = user['reservations'].map((reservation, index) => (
                <tr key={index}>
                    <td>{reservation.voyage.titre}</td>
                    <td>{reservation['optionsSouscrites']}</td>
                    <td>{reservation.montantTotal}</td>
                    <td>A implémenter</td>
                    <td>{reservation.voyage.dateDebut.slice(0,10)}</td>
                    <td>{reservation.nbPlacesRestante}</td>
                    <td>{String(reservation.paiement_autorise)}</td>
                    <td>{String(reservation.paiement_ok)}</td>
                    <td>
                        <Link className={"nav-link"} to={{pathname:"/pay", state: {amount: reservation.montantTotal, id_res : reservation.id} }}>
                            {reservation.button}
                        </Link>
                    </td>
                </tr>
            ))
        }

        const tableHeaders = [
            'Voyage',
            'Options Souscrites',
            'Montant Total',
            'Date de demande',
            'Date de départ',
            'Place restante',
            'Paiement possible',
            'Paiement effectué',
            'Payer'
        ] 

        return (
            <>
                <Nav userLogged={this.state.userLogged} handleDeconnection={this.handleDeconnection}/>

                <div className="d-flex wrapper-profile">
                    <div>
                        <p>Login: {user.login}</p>
                        <p>Email: {user.email}</p>
                        <p>Role: {user.roles}</p>
                    </div>
                
                    <div>
                        <Table responsive>
                            <thead>
                                <tr>
                                {tableHeaders.map((header, index) => (
                                    <th key={index}>{header}</th>
                                ))}
                                </tr>
                            </thead>
                            <tbody>
                                {reservationsJSX}
                            </tbody>
                        </Table>
                    </div>
                </div>
                
                <Footer />
            </>
        )
    }
}

export default Profile;