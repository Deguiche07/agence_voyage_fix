import React, { Component } from 'react'
import Button from 'react-bootstrap/Button'
import {Link} from 'react-router-dom'
import '../../styles/nav.css'

import jwt_decode from 'jwt-decode'

const Nav = ({userLogged, handleDeconnection}) => {

    let admin = false;
    let adminJSX = "";
    let userJSX = "";

    if (userLogged){
        const token_decoded = jwt_decode(localStorage.token);

        if (token_decoded.roles.includes("ROLE_ADMIN")) {
            admin = true;
            adminJSX = (
                <Link className={"nav-link"} to={"/navadmin"}>
                    <Button variant="outline-primary">Admin</Button>
                </Link>
            )
        }
        
        if (token_decoded.roles.includes("ROLE_USER")){
            userJSX = (
                <Link className={"nav-link"} to={{pathname:"/profile", state: {userLogged: 'true', user_id: token_decoded.id}}}>
                    <Button variant="outline-primary" >Profil</Button>
                </Link>
            )
        } else {
            console.log("profil non accessible")
        }
    }

    const buttonsToDisplayJsx = userLogged ?
        <>
        {userJSX}
        {adminJSX}
        <Link  className={"nav-link"} to={{pathname: "/home", state: {userLogged: 'false'}}}>
            <Button variant="outline-danger" onClick={handleDeconnection}>Se déconnecter</Button>
        </Link>

        </> : <>
        
            <Link className={"nav-link"} to={"/signin"}> S'inscrire </Link>
            <Link className={"nav-link"} to={"/login"}>
                <Button variant="outline-primary">Se connecter</Button>
            </Link>
        </>

    return (
        <div className="flex">
            <Link className={"nav-link"} to={"/home"}>
                <img
                    alt=""
                    src="https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Fjournaldesgrandesecoles.com%2Fwp-content%2Fuploads%2Fedhec.png&f=1&nofb=1"
                    width="150"
                    height="75"
                />
            </Link>
            <div className="d-flex align-items-center buttons-style">
                {buttonsToDisplayJsx}
            </div>
        </div>
    )
}

export default Nav